# Bobble Bubble

## Instructions
Use the Arrow Keys to move RIGHT, LEFT and JUMP. Press the Space Bar to throw bubbles towards the enemies, burst the bubbled enemies to kill them.
Bubbles only work against enemies when going horizontally (and are red).
If they all die, you win the level!
Burst bubbles against walls or pick up fruits for extra points. Press P to pause the game... 

GOOD LUCK!

## Credits

Daniela Baraccani, Marco Desiderio, Lorenzo Gardini, Cecilia Teodorani 