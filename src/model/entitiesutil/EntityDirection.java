package model.entitiesutil;

/**
 * All the directions an {@link Entity} can move towards
 */
public enum EntityDirection {
    /**
     * Left direction
     */
    LEFT,
    /**
     * Right direction
     */
    RIGHT,
    /**
     * Up left direction
     */
    UP_LEFT,
    /**
     * Up right direction
     */
    UP_RIGHT,
    /**
     * Down left direction
     */
    DOWN_LEFT,
    /**
     * Down right direction
     */
    DOWN_RIGHT
}

