package model.physics;

/**
 * Represent the direction of {@link Entity}s' collisions.
 */
public enum CollisionDirection {

    /**
     * Collision on the top.
     */
    TOP,

    /**
     * Collision on the bottom.
     */
    BOTTOM,

    /**
     * Collision on the left.
     */
    LEFT,

    /**
     * Collision on the right.
     */
    RIGHT

}
