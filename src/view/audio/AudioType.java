package view.audio;

/**
 * The types of audio tracks.
 */
public enum AudioType {

    /**
     * Effect type.
     */
    EFFECT,

    /**
     * Error type.
     */
    ERROR,

    /**
     * Music type.
     */
    MUSIC;
}
